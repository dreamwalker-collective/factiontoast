## Interface: 80200
## Version: 0.0.1
## Author: Dragonwolf Dreamwalker
## Title: FactionToast
## Notes: Track reputation progress for multiple factions at once.

Libs.xml

LocaleSupport.lua
Localization.lua

FactionToast.lua
FactionToast.xml