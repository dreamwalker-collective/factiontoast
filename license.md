# DON'T BE A JERK PUBLIC LICENSE

> Teen-rated modified DBAD license from https://dbad-license.org/

> Version 1.1, December 2016

> Copyright (C) 2019 Dragonwolf Dreamwalker (aka Shauna Gordon)

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document.

> DON'T BE A JERK PUBLIC LICENSE
> TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

1. Do whatever you like with the original work, just don't be a jerk.

   Being a jerk includes - but is not limited to - the following instances:

 1a. Outright copyright infringement - Don't just copy this and change the name.
 1b. Selling the unmodified original with no work done what-so-ever, that's REALLY being a jerk. (This is also against WoW's TOS, which forbids requiring payment to obtain addons.)
 1c. Modifying the original work to contain hidden harmful content. That would make you a PROPER jerk.

2. If you become rich through modifications, related works/services, or supporting the original work,
share the love. Only a jerk would make loads off this work and not buy the original work's
creator(s) a pint.

3. Code is provided with no warranty. Using somebody else's code and massively complaining when it goes wrong makes you a MAJOR jerk. Fix the problem yourself. A non-jerk would submit the fix back.