# FactionToast

We at The Dreamwalker Collective just adore [Fizzwidget's FactionFriend](http://www.fizzwidget.com/factionfriend), but sometimes, it simply can't keep up with demands of modern diplomacy. While keeping up with one's standing with one race is easy enough with such tools, the world has grown such that one is often having to juggle the reputation of _multiple_ races simultaneously! And that's in addition to monitoring the power level of one's artifact, and in some cases, doing that _while also_ working on gaining one's own life experience!

It's just too much for poor FactionFriend to handle these days.

Enter The Dreamwalker Collective's FactionToast, for advanced diplomatic needs!

The experience bar has gotten too crowded, so we've taken reputation off of it entirely and given it its own space, allowing for tracking reputation gains for _all_ factions involved!

We still <3 FactionFriend, though, so we aim to make sure FactionToast plays well with FactionFriend.

## Features

- Displays distinctly colored bars for _all_ factions involved in an action that gains reputation
- Automatically switches between factions when taking relevant actions (gaining reputation, changing zones, etc)
- Pops up temporarily _or_ stays displayed, depending on setting